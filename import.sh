#!/bin/bash
set -e
if [ "$#" -ne 2 ]; then
  echo "Syntax: ./import.sh <tournament> <games_dir>"
  exit 1
fi

t="$1"
games_dir="$2"

if [ ! -f "tournaments/$t/tournament.yaml" ]; then
  echo "Could not find tournament config for $t"
  exit 3
fi

if [ ! -f "games_to_import.txt" ]; then
  pdm run src/hcc/organize_games.py "$t" "$games_dir" >games_to_import.txt
fi
sets=$(cat games_to_import.txt)
IFS_bkp=$IFS
IFS=$'\n'
echo "Imported sets:"
echo "$sets"
for set_dir in $sets; do
  echo "Importing $set_dir"
  pdm run src/hcc/fill_metadata.py "$t" "$set_dir"
  pdm add-meta "$t" "$set_dir"
  pdm sets "$t"
  pdm add-games "$t" "$set_dir"
  sed -e "/$set_dir/d" <games_to_import.txt >games_to_import_f.txt
  mv games_to_import_f.txt games_to_import.txt
done
IFS=$IFS_bkp
rm games_to_import.txt
pdm games "$t"
pdm ex "$t"
