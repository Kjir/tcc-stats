#!/bin/bash
set -e
if [ "$#" -ne 3 ]; then
  echo "Syntax: ./run_all.sh <tournament> <zipfile> <set_dir>"
  exit 1
fi

t="$1"
zipfile="$2"
set_dir="$3"
g="tournaments/$t/games"

if [ ! -f "tournaments/$t/tournament.yaml" ]; then
  echo "Could not find tournament config for $t"
  exit 3
fi

if [ ! -f "$zipfile" ]; then
  echo "Zipfile $zipfile not found"
  exit 2
fi

mkdir -p "$g/$set_dir"
unzip "$zipfile" -d "$g/$set_dir"
pdm run src/hcc/fill_metadata.py "$t" "$set_dir"
pdm add-meta "$t" "$set_dir"
pdm sets "$t"
pdm add-games "$t" "$set_dir"
pdm games "$t"
rm "$zipfile"
