#!/bin/bash
set -e
if [ "$#" -ne 2 ]; then
  echo "Syntax: ./import.sh <tournament> <set_dir>"
  exit 1
fi

t="$1"
set_dir="$2"
g="tournaments/$t/games"

if [ ! -f "tournaments/$t/tournament.yaml" ]; then
  echo "Could not find tournament config for $t"
  exit 3
fi

pdm run src/hcc/fill_metadata.py "$t" "$set_dir"
pdm add-meta "$t" "$set_dir"
pdm sets "$t"
pdm add-games "$t" "$set_dir"
pdm games "$t"
pdm ex "$t"
