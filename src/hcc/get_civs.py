from sys import argv
from ratelimit import limits, RateLimitException
from backoff import on_exception, expo
import requests


@on_exception(expo, RateLimitException, max_tries=8)
@limits(calls=2, period=1)
def get_civs(draft_id: str):
    print(f"Getting: {draft_id}")
    response = requests.get(f"https://aoe2cm.net/api/draft/{draft_id}")
    try:
        return response.json()
    except Exception:
        print(response.text)
        raise


if __name__ == "__main__":
    print(argv)
    get_civs(argv[1])
