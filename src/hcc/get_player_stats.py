from sys import argv
from ratelimit import limits, RateLimitException
from backoff import on_exception, expo
import requests
import pandas


@on_exception(expo, RateLimitException, max_tries=8)
@limits(calls=2, period=1)
def get_player_stats(player_id: str):
    print(f"Getting {player_id}")
    response = requests.get(
        f"https://aoe-api.worldsedgelink.com/community/leaderboard/GetPersonalStat?title=age2&profile_ids=[%22{player_id}%22]"
    )
    return response.json()


@on_exception(expo, RateLimitException, max_tries=8)
@limits(calls=5, period=1)
def get_player_id(insights_id: str):
    print(f"Getting {insights_id}")
    response = requests.get(
        f"https://www.aoe2insights.com/user/{insights_id}/relic-id/"
    )
    print(f"{insights_id} = {response.json()}")
    return response.json()


def get_player_country(player_id: str):
    stats = get_player_stats(player_id)
    if stats["result"]["code"] == 6:
        print(f"Player {player_id} not found")
        return None
    return stats["statGroups"][0]["members"][0]["country"]


def extract_player_id(players):
    return (
        players["player_profile"]
        .str.replace(
            r"https?://(www.)?aoe2insights.com/user/([^/]*)/?(.*)?", "\\2", regex=True
        )
        .map(get_player_id)
    )


if __name__ == "__main__":
    players = pandas.read_csv(argv[1], header=0)
    players["id"] = extract_player_id(players)
    players["country"] = players["id"].map(get_player_country)
    players.to_csv(argv[1], header=True, index=False)
