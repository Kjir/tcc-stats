import os.path
import sys

import pandas

from hcc.players import rename_alias


def enrich_games(games: pandas.DataFrame, sets: pandas.DataFrame) -> pandas.DataFrame:
    normalize = lambda p: p.lower().strip() if p is not None else ""

    games["players"] = (
        games[["host_name", "guest_name"]]
        .map(normalize)
        .agg(lambda row: "-".join(sorted(row)), axis="columns")
    )
    sets["players"] = (
        sets[["player1", "player2"]]
        .map(normalize)
        .agg(lambda row: "-".join(sorted(row)), axis="columns")
    )

    unknown_games = games[~games["players"].isin(sets["players"])]
    if len(unknown_games) > 0:
        print(
            unknown_games[
                [
                    "host_name",
                    "guest_name",
                    "map",
                    "civ1",
                    "civ2",
                    "winner",
                    "set",
                    "file",
                ]
            ]
        )
        raise Exception("Found games that could not be matched to sets")

    sets_and_games = sets.merge(
        games,
        how="left",
        left_on="players",
        right_on="players",
        suffixes=["_set", "_game"],
    )
    set_counts = (
        sets_and_games[pandas.notna(sets_and_games["full_map_draft"])]
        .groupby("players")["set"]
        .nunique()
    )
    rematches = sets_and_games[
        sets_and_games["players"].isin(set_counts[set_counts > 1].index)
    ].sort_values("date", ascending=True)
    set_dates = (
        rematches.groupby(["players", "set"])
        .apply(lambda s: s["date"], include_groups=False)
        .drop_duplicates()
    )
    sets_and_games = sets_and_games[
        sets_and_games.apply(
            is_game_in_set(set_dates),
            axis="columns",
        )
    ]

    # Remove info about the finals in the early week sets
    # sets_and_games = sets_and_games[
    #     (sets_and_games["week"] > 3)
    #     | (sets_and_games["date"].map(lambda d: d.date()) <= datetime.date(2024, 4, 9)) | pandas.isna(sets_and_games['date'])
    # ]
    # Remove info about group matches in the final sets
    # sets_and_games = sets_and_games[
    #     (sets_and_games["week"] < 6)
    #     | (sets_and_games["date"].map(lambda d: d.date()) > datetime.date(2024, 4, 9)) | pandas.isna(sets_and_games['date'])
    # ]

    def rename_player(row):
        if pandas.isna(row["winner"]):
            return row["winner"]
        return rename_alias(row["winner"], [row["host_name"], row["guest_name"]])

    wins = (
        sets_and_games.assign(
            winner=sets_and_games.apply(
                rename_player,
                axis="columns",
            ).map(normalize, na_action="ignore")
        )
        .groupby(["players", "stage"])
        .value_counts(["winner"])
    )

    def get_wins(row, player):
        try:
            return wins.loc[(row["players"], row["stage"], normalize(row[player]))]
        except KeyError:
            return 0

    sets_and_games["win1"] = sets_and_games.apply(
        get_wins,
        args=("player1_set",),
        axis="columns",
    )
    sets_and_games["win2"] = sets_and_games.apply(
        get_wins,
        args=("player2_set",),
        axis="columns",
    )
    return sets_and_games


def is_game_in_set(set_dates: pandas.DataFrame):
    def _is_game_in_set(set_info):
        if set_info["players"] not in set_dates.index.levels[0]:
            return True
        first_set = (
            set_dates[set_info["players"]]
            .sort_values(ascending=True)
            .reset_index()
            .head(1)["set"]
            .values[0]
        )
        second_set = (
            set_dates[set_info["players"]]
            .sort_values(ascending=True)
            .reset_index()
            .tail(1)["set"]
            .values[0]
        )
        if set_info["stage"].startswith("Group"):
            return set_info["set"] == first_set
        return set_info["set"] == second_set

    return _is_game_in_set


def get_top_by(games, group, counts, n=5):
    return games.groupby(group).apply(
        lambda x: x.value_counts(counts).nlargest(n), include_groups=False
    )


def get_win_rate_by(games, group=None):
    games = games[pandas.notna(games["winner"])]
    played_count_group = games.explode("civs")
    if group is not None:
        played_count_group = played_count_group.groupby(group)
    played_count = played_count_group.value_counts(["civs"])
    win_counts_group = games
    if group is not None:
        win_counts_group = win_counts_group.groupby(group)

    win_counts = win_counts_group.value_counts(["win_civ"])
    if group is None:
        colnames = ["civ", "rate"]
        sortcols = ["rate"]
        ascending = [False]
        index = ["civ"]
    else:
        colnames = [group, "civ", "rate"]
        sortcols = [group, "rate"]
        ascending = [True, False]
        index = [group, "civ"]
    return (
        pandas.DataFrame({"count": played_count, "wins": win_counts})
        .fillna(0)
        .agg(lambda row: row["wins"] / row["count"] * 100, axis="columns")
        .reset_index()
        .set_axis(colnames, axis="columns")
        .sort_values(sortcols, ascending=ascending)
        .set_index(index)
        .map(lambda v: f"{round(v, 2)}%")
    )


def print_civ_stats(games):
    with pandas.option_context("display.max_columns", None, "display.max_rows", None):
        games["civs"] = games.agg(
            lambda row: [row["civ1"], row["civ2"]], axis="columns"
        )
        games["win_civ"] = games.agg(
            lambda row: (
                row["civ1"] if row["winner"] == row["player1_game"] else row["civ2"]
            ),
            axis="columns",
        )
        games.loc[pandas.isna(games["winner"]), "win_civ"] = None
        print("\n Most played civs")
        print(games.explode("civs").value_counts("civs"))
        print("\nMost played civs by bracket")
        played_by_bracket = get_top_by(games.explode("civs"), "bracket", ["civs"])
        print(played_by_bracket)
        print("\nMost played civs by stage")
        print(get_top_by(games.explode("civs"), "stage", ["civs"]))
        print("\nMost played civs by map")
        print(get_top_by(games.explode("civs"), "map", ["civs"]))
        print("\nCiv win rate")
        print(get_win_rate_by(games))
        print("\nCiv win rate by bracket")
        print(get_win_rate_by(games, "bracket"))
        print("\nCiv win rate by stage")
        print(get_win_rate_by(games, "stage"))
        print("\nCiv win rate by map")
        print(get_win_rate_by(games, "map"))


def print_map_stats(games):
    with pandas.option_context("display.max_columns", None, "display.max_rows", None):
        print("\n Most played maps")
        print(games.value_counts(["map"]))
        print("\nMost played maps by bracket")
        print(get_top_by(games, "bracket", ["map"]))
        print("\nMost played maps by stage")
        print(get_top_by(games, "stage", ["map"]))


def print_duration_stats(games):
    with pandas.option_context("display.max_columns", None, "display.max_rows", None):
        print("\n Shortest games")
        print(
            games[
                games["file"].map(lambda n: pandas.isna(n) or "part 1" not in n)
            ].nsmallest(5, "duration")[
                [
                    "player1_set",
                    "player2_set",
                    "bracket",
                    "stage",
                    "map",
                    "duration",
                    "winner",
                ]
            ]
        )
        print("\n Longest games")
        print(
            games.nlargest(5, "duration")[
                ["player1_set", "player2_set", "bracket", "stage", "map", "duration"]
            ]
        )


def main():
    tournament = sys.argv[1]
    os.chdir(os.path.join("tournaments", tournament))
    games = pandas.read_parquet("games.parquet")
    sets = pandas.read_parquet("sets.parquet")
    # print(sets[sets.bracket == "Light Cavs"])
    enriched_games = enrich_games(games, sets)
    enriched_games.to_json("sets_and_games.json")
    # with pandas.option_context("display.max_columns", None, "display.max_rows", None):
    # print(enriched_games[pandas.isna(enriched_games.set)][["player1_set", "player2_set", "bracket", "stage", "win1", "win2"]])
    # print(enriched_games[(enriched_games.win1 <2) & (enriched_games.win2 < 2)].groupby(["player1_set", "player2_set"]).first().reset_index()[["player1_set", "player2_set", "bracket", "stage", "win1", "win2"]])
    # print(enriched_games[pandas.notna(enriched_games['set']) | pandas.notna(enriched_games['map_draft'])][["player1_set", "player2_set", "duration", "civ1", "civ2", "map", "bracket", "stage", "winner", "set"]])
    # print_map_stats(enriched_games)
    # print_civ_stats(enriched_games)
    # print_duration_stats(enriched_games)


if __name__ == "__main__":
    main()
