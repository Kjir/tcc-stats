from sys import argv
import datetime
from mgz.reference import get_dataset
from mgz.summary import Summary
from mgz.util import Version

_, civs = get_dataset(Version.DE, 0)
civs = civs["civilizations"]

with open(argv[1], "rb") as f:
    s = Summary(f)
    map = s.get_map()
    print(f"Map: {map['name']}")
    # print(s.get_teams())
    players = s.get_players()
    duration = s.get_duration()
    winner = None
    player_summary = (
        lambda player: f"{player['color_id']} - {player['name']} ({civs.get(str(player['civilization']), {'name': '<unknown>'})['name']}) [{'W' if player['winner'] else 'L'}] <{player['eapm']}>"
    )
    print(f"{player_summary(players[0])} vs {player_summary(players[1])}")
    # if players[0]
    # print(s.get_duration())
    print(datetime.datetime.fromtimestamp(s.get_played()))
    print(f"Total Duration: {s.get_duration()}")
    print(f"Completed: {s.get_completed()}")
    print(f"Restored: {s.get_restored()}")
    print("Chat:")
    for message in s.get_chat():
        if message["origination"] == "game":
            continue
        print(
            f"<{datetime.timedelta(milliseconds=message['timestamp'])}> {players[message['player_number'] - 1]['name']}: {message['message']}"
        )
    print(s.get_settings())
    # print(s.get_chat())
    # print(json.dumps(players))
