from dataclasses import dataclass
from datetime import datetime
import json
import os
from pathlib import Path
import py7zr
import rarfile
import re
import shutil
import sys
from tempfile import TemporaryDirectory
import zipfile

from mgz.summary import Summary

from hcc.tournament import get_tournament_maps, get_tournament_maps_presets


@dataclass
class ShortRecInfo:
    path: Path
    timestamp: datetime
    player1: str
    player2: str
    player_count: int
    map: str


def organize_games(games_dir: Path):
    processed_games = get_processed_games()
    files_to_pick = find_file_candidates(games_dir, processed_games)
    added_dirs: set[str] = set()
    for game in files_to_pick:
        processed, new_dir = organize_game(game)
        if processed:
            processed_games.append(str(game))
        if new_dir is not None:
            added_dirs.add(new_dir)
    save_processed_games(processed_games)
    return added_dirs


def organize_game(game: Path):
    if game.suffix == ".aoe2record":
        return organize_single_game(game)
    elif game.suffix in [".zip", ".rar", ".7z"]:
        return organize_archive(game)
    else:
        raise Exception(f"Unknown file type {game.suffix}")


def organize_single_game(game: Path):
    tournament_maps = get_tournament_maps()
    match = get_rec_info(game)
    if match is None:
        print(f"Cannot parse {game}", file=sys.stderr)
        return False, None
    if match.map not in tournament_maps:
        return False, None
    destination = Path(
        "games",
        f"{match.timestamp.strftime('%Y%m%d')} {match.player2.replace('/', '_')} vs {match.player1.replace('/', '_')}",
    )
    if not destination.exists():
        destination = Path(
            "games",
            f"{match.timestamp.strftime('%Y%m%d')} {match.player1.replace('/', '_')} vs {match.player2.replace('/', '_')}",
        )
    if re.match(".*[gG]\\d\\W.*", match.path.name):
        destination = Path(destination, match.path.name)
    else:
        destination = Path(
            destination,
            f"{match.timestamp.strftime('%H%M%S')} {match.player1.replace('/', '_')} vs {match.player2.replace('/', '_')}.aoe2record",
        )
    if destination.exists():
        print(f"{destination} exists, skipping...", file=sys.stderr)
        return True, None
    destination.parent.mkdir(parents=True, exist_ok=True)
    shutil.copy(match.path, destination)
    # game.unlink()
    return True, str(destination.parent.name)


def organize_archive(archive: Path):
    with TemporaryDirectory() as tmpdir:
        get_archive_info(archive).extractall(tmpdir)
        match = get_last_match_for_tournament(Path(tmpdir))
        if match is None:
            return False, None
        game_dir = Path(
            "games",
            f"{match.timestamp.strftime('%Y%m%d')} {match.player1.replace('/', '_')} vs {match.player2.replace('/', '_')}",
        )
        if game_dir.exists():
            print(f"{game_dir} exists skipping...", file=sys.stderr)
            return True, None
        shutil.copytree(match.path.parent, game_dir)
        # archive.unlink()
        return True, str(game_dir.name)


def get_last_match_for_tournament(tmpdir: Path):
    map_presets = get_tournament_maps_presets()
    tournament_maps = get_tournament_maps()
    metadata_file = Path(tmpdir, "metadata.json")
    if metadata_file.exists():
        with metadata_file.open() as meta:
            metadata = json.load(meta)
            if metadata.get("maps") is not None:
                if metadata["maps"]["preset"] not in map_presets:
                    return None
    first_rec = None
    for game in Path(tmpdir).glob("**/*.aoe2record"):
        info = get_rec_info(game)
        if info is not None:
            if info.map in tournament_maps:
                if first_rec is None or first_rec.timestamp < info.timestamp:
                    first_rec = info
            else:
                print(f"{game.name} - Unrecognized map {info.map}", file=sys.stderr)
    return first_rec


def get_rec_info(rec: Path):
    try:
        info = Summary(rec.open("rb"))
        return ShortRecInfo(
            rec,
            datetime.fromtimestamp(info.get_played() or 0),
            info.get_players()[0]["name"],
            info.get_players()[1]["name"],
            len(info.get_players()),
            info.get_map()["name"],
        )
    except Exception:
        print(f"Failed to parse {rec}", file=sys.stderr)


def find_file_candidates(games_dir: Path, processed_games: list[str] | None = None):
    if processed_games is None:
        processed_games = []
    files_to_pick: list[Path] = []
    extensions = ["zip", "rar", "7z"]
    for extension in extensions:
        for archive in games_dir.glob(f"**/*.{extension}"):
            if str(archive) in processed_games:
                continue
            if should_pick_archive(archive):
                files_to_pick.append(archive)

    for rec in games_dir.glob("**/*.aoe2record"):
        if str(rec) not in processed_games:
            files_to_pick.append(rec)
    return files_to_pick


def get_archive_info(archive: Path):
    if archive.suffix == ".zip":
        return zipfile.ZipFile(archive)
    elif archive.suffix == ".rar":
        return rarfile.RarFile(archive)
    elif archive.suffix == ".7z":
        return py7zr.SevenZipFile(archive)
    else:
        raise Exception(f"Unsupported extension: {archive.suffix}")


def should_pick_archive(archive: Path):
    archive_info = get_archive_info(archive)
    if getattr(archive_info, "comment", "") in [
        b"Generated by https://zetatwo.github.io/aoe2replaypacker",
        b"Generated by https://replaypacker.zeta-two.com",
    ]:
        return True
    for zipped_file in archive_info.namelist():
        if not (zipped_file.endswith(".aoe2record") or zipped_file.endswith("/")):
            return False
    return True


def get_processed_games() -> list[str]:
    file_path = Path("processed_files.json")
    if not file_path.exists():
        return []
    with file_path.open() as processed_files:
        return json.load(processed_files)


def save_processed_games(processed_games: list[str]):
    file_path = Path("processed_files.json")
    with file_path.open("w") as processed_files:
        json.dump(processed_games, processed_files)


if __name__ == "__main__":
    tournament = Path("tournaments", sys.argv[1])
    games_dir = Path(sys.argv[2])
    os.chdir(tournament)
    added_dirs = organize_games(games_dir)
    print("\n".join(added_dirs))
