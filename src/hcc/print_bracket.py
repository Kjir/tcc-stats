import datetime
import os.path
import sys
import pandas


def print_bracket(tournament: str, bracket: str):
    sets_games = pandas.read_json(
        os.path.join("tournaments", tournament, "sets_and_games.json")
    )
    played_filter = (pandas.notna(sets_games["player1_game"])) & (
        pandas.notna(sets_games["player2_set"])
    )
    cols = [
        "player1_set",
        "player2_set",
        "stage",
        "eapm1",
        "eapm2",
        "map",
        "duration",
        "winner",
        "win1",
        "win2",
        "civ1",
        "civ2",
    ]
    sets_to_print = sets_games[played_filter & (sets_games["bracket"] == bracket)][cols]
    sets_to_print["duration"] = sets_to_print["duration"].map(
        lambda x: datetime.timedelta(milliseconds=x)
    )
    with pandas.option_context("display.max_rows", None):
        print(sets_to_print)


if __name__ == "__main__":
    tournament = sys.argv[1]
    bracket = sys.argv[2]
    print_bracket(tournament, bracket)
