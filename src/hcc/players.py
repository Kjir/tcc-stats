from enum import Enum
import jellyfish
import os
import pandas
import sys
import yaml


class FindOrAddResult(Enum):
    ADDED = 1
    NO_ACTION = 2
    OTHER_ACTION = 3


def load_aliases():
    with open("player_aliases.yaml", "r") as f:
        return yaml.safe_load(f)


def add_alias(alias: str, name: str):
    aliases = load_aliases()
    aliases[alias.lower().strip()] = name
    with open("player_aliases.yaml", "w") as f:
        yaml.dump(aliases, f)


def rename_alias(name: str, possible_names: list[str] | None = None):
    player_aliases: dict[str, str | list[str]] = load_aliases()
    alias = player_aliases.get(name.strip().lower(), name)
    if isinstance(alias, list):
        if possible_names is not None:
            for a in alias:
                if a in possible_names:
                    return a
        selected_index = len(alias) + 1
        print(f'Multiple aliases found for "{name}":')
        for index, a in enumerate(alias):
            print(f'{index+1}. "{a}"')
        while selected_index > len(alias):
            selected_index = int(input("Select the correct alias: "))
            if selected_index > len(alias):
                print(f"Please give an index between 1 and {len(alias)}")
                continue
            if selected_index < 1:
                raise Exception("Exiting")
            alias = alias[selected_index - 1]
    if isinstance(alias, list):
        raise Exception("Alias is still a list, this should not happen")
    return alias


def _load_sets():
    if os.path.isfile("sets.parquet"):
        return pandas.read_parquet("sets.parquet")
    else:
        return pandas.read_csv("sets.csv")


def get_player_sets(name: str):
    sets = _load_sets()
    name = name.lower()
    return sets[
        (sets["player1"].str.lower() == name) | (sets["player2"].str.lower() == name)
    ]


def find_similar_names(name: str):
    sets = _load_sets()
    similarity = sets["player1"].map(
        lambda p: jellyfish.jaro_winkler_similarity(p, name), na_action="ignore"
    )
    return sets.player1[similarity > 0.7].unique().tolist()


def find_or_add_alias(
    name: str, opponent: str | None = None, *, other: str = "Other action"
) -> FindOrAddResult:
    aliased = rename_alias(name)
    aliased_opponent = rename_alias(opponent) if opponent is not None else None
    player_sets = get_player_sets(aliased)
    if len(player_sets) > 0:
        # Player name was found, alias not needed
        return FindOrAddResult.NO_ACTION
    similar = find_similar_names(aliased)
    if len(similar) == 0 and aliased_opponent is not None:
        opponent_sets = get_player_sets(aliased_opponent)
        similar = pandas.concat([opponent_sets.player1, opponent_sets.player2])
        similar = (
            similar[similar.str.lower() != aliased_opponent.lower()].unique().tolist()
        )
    print(f"Player {aliased} not found.")
    if len(similar) > 0:
        print("Here are the possible names:")
        for index, s in enumerate(similar):
            print(f"{index+1}. {s}")
        print(f"-1. {other}")
    original_name = input(f"Type a number or the alias for {aliased}: ")
    if original_name == "-1":
        return FindOrAddResult.OTHER_ACTION
    if original_name.isdigit() and len(similar) > 0:
        selected_index = int(original_name)
        if selected_index > len(similar):
            print(f"Please select an option from 1 to {len(similar)}")
            return FindOrAddResult.NO_ACTION
        original_name = similar[selected_index - 1]
    elif len(get_player_sets(original_name)) == 0:
        print(f'The alias "{original_name}" cannot be found among the players')
        return FindOrAddResult.NO_ACTION
    print(f"New alias is {original_name}")
    add_alias(name, original_name)
    return FindOrAddResult.ADDED


if __name__ == "__main__":
    tournament = sys.argv[1]
    os.chdir(os.path.join("tournaments", tournament))
    if len(sys.argv) == 3:
        find_or_add_alias(sys.argv[2])
    elif len(sys.argv) == 4:
        find_or_add_alias(sys.argv[2], sys.argv[3])
    else:
        print(f"Usage: {sys.argv[0]} <tournament> <player_name> [<player_opponent>]")
        sys.exit(-1)
