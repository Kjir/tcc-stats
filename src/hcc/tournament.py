import yaml


def get_tournament_maps():
    tournament = get_tournament_info()
    return tournament["maps"]


def get_tournament_bracket_ranks():
    tournament = get_tournament_info()

    return {bracket: index for (index, bracket) in enumerate(tournament["brackets"])}


def get_tournament_maps_presets():
    tournament = get_tournament_info()
    return tournament.get("presets", {}).get("maps", {}).values()


def get_tournament_civs_presets():
    tournament = get_tournament_info()
    return tournament.get("presets", {}).get("civs", {}).values()


def get_tournament_info():
    with open("tournament.yaml") as t_info:
        return yaml.full_load(t_info)
