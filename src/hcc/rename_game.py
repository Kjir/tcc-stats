from pathlib import Path
from sys import argv
import datetime
from mgz.reference import get_dataset
from mgz.summary import Summary
from mgz.util import Version

_, civs = get_dataset(Version.DE, 0)
civs = civs["civilizations"]


def rename_game(file_path: Path):
    with file_path.open("rb") as f:
        s = Summary(f)
        game_date = datetime.datetime.fromtimestamp(s.get_played())
        teams = s.get_teams()
        players = s.get_players()
        map_info = s.get_map()
        new_name = ""
        if len(teams) == 2:
            if len(players) == 2:
                new_name = f"{game_date.strftime('%Y%m%d%H%M%S')}_{players[0]['name']}_vs_{players[1]['name']}_on_{map_info['name']}.aoe2record"
            else:
                print("2v2")
        elif len(teams) == len(players):
            print("FFA")
        else:
            print(f"{len(teams)}v{len(teams)}")
    if new_name == "":
        print("Not implemented yet")
        return

    answer = input(f'Would you like to rename the game to "{new_name}"? [Y/n] ')
    if answer.lower() in ["", "y"]:
        _ = file_path.rename(file_path.with_name(new_name))


if __name__ == "__main__":
    file_path = Path(argv[1])
    rename_game(file_path)
