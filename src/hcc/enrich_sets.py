import os.path
import sys

import pandas

from hcc.get_civs import get_civs
from hcc.players import rename_alias
from hcc.civilizations import civilizations


def extract_draft_id(draft_url: str) -> str:
    return draft_url.replace("https://aoe2cm.net/draft/", "").strip()


def extract_events(
    draft: pandas.Series,
):
    previous_offset = draft["startTimestamp"]
    selected_events = []
    for event in draft["events"]:
        if "action" in event:
            continue
        selected_events.append({**event, "time": event["offset"] - previous_offset})
        previous_offset = event["offset"]
    return selected_events


def normalize_name(name: str):
    return rename_alias(name).strip().lower()


def check_errors(row):
    if pandas.isna(row["map_draft"]):
        return pandas.NA
    errors: dict[str, list[str]] = {"errors": [], "warnings": []}
    map = row["full_map_draft"]
    civ = row["full_civ_draft"]
    host_player = row["player1"]
    guest_player = row["player2"]
    map_host = row["map_draft_host"]
    map_guest = row["map_draft_guest"]
    if row["civ_draft_host"] == row["map_draft_host"]:
        civ_host = map_host
    else:
        civ_host = row["civ_draft_host"]
    if row["civ_draft_guest"] == row["map_draft_guest"]:
        civ_guest = map_guest
    else:
        civ_guest = row["civ_draft_guest"]

    if map_host != civ_host:
        errors["errors"].append(
            f"Host does not match between drafts: {map_host} - {civ_host}"
        )
    if map_guest != civ_guest:
        errors["errors"].append(
            f"Guest does not match between drafts: {map_guest} - {civ_guest}"
        )

    if (
        map_host == civ_host
        and map_guest == civ_guest
        and host_player == map_guest
        and guest_player == map_host
    ):
        errors["errors"].append("Host and Guest have been switched around")

    if map_host != host_player:
        errors["warnings"].append(f"Unknown Map Host: {map_host}")
    if civ_host != host_player:
        errors["warnings"].append(f"Unknown Civ Host: {civ_host}")
    if map_guest != guest_player:
        errors["warnings"].append(f"Unknown Civ Guest: {map_guest}")
    if civ_guest != guest_player:
        errors["warnings"].append(f"Unknown Civ Guest: {civ_guest}")

    if len(errors["errors"]) == 0 and len(errors["warnings"]) == 0:
        return None
    return errors


def fetch_drafts(sets: pandas.DataFrame):
    sets["civ_draft"] = sets["civ_draft"].map(extract_draft_id, na_action="ignore")
    sets["map_draft"] = sets["map_draft"].map(extract_draft_id, na_action="ignore")
    sets["full_civ_draft"] = sets["civ_draft"].map(get_civs, na_action="ignore")
    sets["full_map_draft"] = sets["map_draft"].map(get_civs, na_action="ignore")
    sets.to_parquet("sets.parquet")
    return sets


def fetch_missing_drafts(sets: pandas.DataFrame):
    sets["civ_draft"] = sets["civ_draft"].map(extract_draft_id, na_action="ignore")
    sets["map_draft"] = sets["map_draft"].map(extract_draft_id, na_action="ignore")
    missing_map_filter = pandas.isna(sets["full_map_draft"]) & pandas.notna(
        sets["map_draft"]
    )
    missing_civ_filter = pandas.isna(sets["full_civ_draft"]) & pandas.notna(
        sets["civ_draft"]
    )
    sets.loc[missing_civ_filter, "full_civ_draft"] = sets.loc[missing_civ_filter][
        "civ_draft"
    ].map(get_civs, na_action="ignore")
    sets.loc[missing_map_filter, "full_map_draft"] = sets.loc[missing_map_filter][
        "map_draft"
    ].map(get_civs, na_action="ignore")
    sets.to_parquet("sets.parquet")
    return sets


def print_draft_mistakes_stats(sets: pandas.DataFrame):
    sets["errors"] = sets["draft_errors"].map(lambda x: x["errors"], na_action="ignore")
    with pandas.option_context("display.max_colwidth", None, "display.max_rows", None):
        sets_with_errors = sets[
            sets["draft_errors"].map(lambda x: pandas.notna(x) and len(x["errors"]) > 0)
        ]
        print(sets_with_errors[["player1", "player2", "bracket", "stage", "errors"]])
        drafts_played = sets[pandas.notna(sets["full_map_draft"])].shape[0]
        print(
            f"{drafts_played} drafts were played out of {sets.shape[0]} total ({round(drafts_played / sets.shape[0] * 100, 2)}%)."
        )
        print(
            f"There are {sets_with_errors.shape[0]} wrong drafts out of {drafts_played} ({round(sets_with_errors.shape[0] / drafts_played * 100.0, 2)}%)."
        )

        sets_switched = sets[
            sets["draft_errors"].map(
                lambda x: pandas.notna(x)
                and len(x["errors"]) == 1
                and x["errors"][0] == "Host and Guest have been switched around"
            )
        ].shape[0]
        print(
            f"There are {sets_switched} sets where host and guest were switched around"
        )
        bracket_errors = sets.groupby("bracket")["draft_errors"].agg(
            num_err=lambda errors: sum(pandas.notna(errors)),
            set_count=lambda errors: len(errors),
        )
        bracket_errors["percent"] = round(
            bracket_errors["num_err"] / bracket_errors["set_count"] * 100, 2
        )
        print(bracket_errors.sort_values(by="percent", ascending=False))
        bracket_errors = sets.groupby("week")["draft_errors"].agg(
            num_err=lambda errors: sum(pandas.notna(errors)),
            set_count=lambda errors: len(errors),
        )
        bracket_errors["percent"] = round(
            bracket_errors["num_err"] / bracket_errors["set_count"] * 100, 2
        )
        print(bracket_errors.sort_values(by="percent", ascending=False))
        print(
            sets[
                (sets["week"] == 6)
                & sets["draft_errors"].map(
                    lambda x: pandas.notna(x) and len(x["errors"]) > 0
                )
            ]
        )
        print(sets.columns)


def print_top_action(sets: pandas.DataFrame, action: str, group=None, admin=False):
    if admin:
        sets = sets[sets["executingPlayer"] == "NONE"]
    else:
        sets = sets[sets["executingPlayer"] != "NONE"]
    if group is not None:
        top_events = (
            sets[sets["actionType"] == action]
            .groupby(group)
            .apply(
                lambda x: x.value_counts(["chosenOptionId"]).nlargest(3),
                include_groups=False,
            )
        )
        print(f"\nMost {action}s by {group}: \n{top_events}")
    else:
        top_events = (
            sets[sets["actionType"] == action]
            .value_counts("chosenOptionId")
            .nlargest(10)
        )
        print(f"\nMost {action}s: \n{top_events}")


def count_bottom(set: pandas.DataFrame, n=7):
    selected_options = set.value_counts(["chosenOptionId"])
    values = []
    for option in set["options"].iloc[0]:
        values.append(selected_options.get(option, 0))
    all_options = pandas.Series(values, index=set["options"].iloc[0])
    return all_options.nsmallest(n)


def print_bottom_action(sets: pandas.DataFrame, action: str, group=None, admin=False):
    if admin:
        sets = sets[sets["executingPlayer"] == "NONE"]
    else:
        sets = sets[sets["executingPlayer"] != "NONE"]
    if group is not None:
        bottom_events = (
            sets[sets["actionType"] == action]
            .groupby(group)
            .apply(
                count_bottom,
                include_groups=False,
            )
        )
        print(f"\nLeast {action}s by {group}: \n{bottom_events}")
    else:
        bottom_events = count_bottom(sets[sets["actionType"] == action], 10)
        print(f"\nLeast {action}s: \n{bottom_events}")


def print_random_actions(sets: pandas.DataFrame, action: str):
    sets = sets[sets["executingPlayer"] != "NONE"]
    print()
    print(f"Random {action}s\n")
    total_events = sets[
        pandas.notna(sets["isRandomlyChosen"]) & (sets["actionType"] == action)
    ].shape[0]
    print(sets[sets["actionType"] == action].value_counts("isRandomlyChosen"))
    print("Percentage:")
    print(
        sets[sets["actionType"] == action].value_counts("isRandomlyChosen")
        / total_events
        * 100
    )
    print()
    print("The following players have explicitly picked random without a timeout")
    print(
        sets[
            (sets["actionType"] == action)
            & (sets["isRandomlyChosen"] == True)
            & (sets["time"] < 30000)
        ]
    )
    print_top_action(sets[sets["isRandomlyChosen"] == True], action)


def print_civ_stats(sets: pandas.DataFrame):
    sets["events"] = sets["full_civ_draft"].map(extract_events, na_action="ignore")
    sets["options"] = [civilizations for _ in range(sets.shape[0])]
    sets = sets.explode("events")
    sets = pandas.concat(
        [sets.drop("events", axis=1), sets["events"].apply(pandas.Series)], axis=1
    )
    with pandas.option_context("display.max_colwidth", None, "display.max_rows", None):
        print("Civ draft stats")
        # Overall
        print_top_action(sets, "ban")
        print_bottom_action(sets, "ban")
        print_top_action(sets, "pick")
        print_bottom_action(sets, "pick")

        # By bracket
        print_top_action(sets, "ban", "bracket")
        print_top_action(sets, "pick", "bracket")
        print_bottom_action(sets, "pick", "bracket")

        # By week
        print_top_action(sets, "ban", "week")
        print_bottom_action(sets, "ban", "week")
        print_top_action(sets, "pick", "week")
        print_bottom_action(sets, "pick", "week")

        # By stage
        print_top_action(sets, "ban", "stage")
        print_bottom_action(sets, "ban", "stage")
        print_top_action(sets, "pick", "stage")
        print_bottom_action(sets, "pick", "stage")

        # Random bans
        print_random_actions(sets, "ban")

        # Random picks
        print_random_actions(sets, "pick")


def print_map_stats(sets: pandas.DataFrame):
    sets["events"] = sets["full_map_draft"].map(extract_events, na_action="ignore")
    sets["options"] = sets["full_map_draft"].map(
        lambda draft: [option["id"] for option in draft["preset"]["draftOptions"]],
        na_action="ignore",
    )
    sets = sets.explode("events")
    sets = pandas.concat(
        [sets.drop("events", axis=1), sets["events"].apply(pandas.Series)], axis=1
    )
    with pandas.option_context("display.max_colwidth", None, "display.max_rows", None):
        print("Map draft stats")
        # Overall
        print_top_action(sets, "ban")
        print_bottom_action(sets, "ban")
        print_top_action(sets, "pick")
        print_bottom_action(sets, "pick")

        print("\nAdmin pick")
        print_top_action(sets, "pick", admin=True)
        print_bottom_action(sets, "pick", admin=True)

        # By bracket
        print_top_action(sets, "ban", "bracket")
        print_bottom_action(sets, "ban", "bracket")
        print_top_action(sets, "pick", "bracket")
        print_bottom_action(sets, "pick", "bracket")

        print("\nAdmin pick")
        print_top_action(sets, "pick", "bracket", admin=True)
        print_bottom_action(sets, "pick", "bracket", admin=True)

        # By week
        print_top_action(sets, "ban", "week")
        print_bottom_action(sets, "ban", "week")
        print_top_action(sets, "pick", "week")
        print_bottom_action(sets, "pick", "week")

        print("\nAdmin pick")
        print_top_action(sets, "pick", "week", admin=True)
        print_bottom_action(sets, "pick", "week", admin=True)

        # By stage
        print_top_action(sets, "ban", "stage")
        print_bottom_action(sets, "ban", "stage")
        print_top_action(sets, "pick", "stage")
        print_bottom_action(sets, "pick", "stage")

        print("\nAdmin pick")
        print_top_action(sets, "pick", "stage", admin=True)
        print_bottom_action(sets, "pick", "stage", admin=True)

        # Random bans
        print_random_actions(sets, "ban")

        # Random picks
        print_random_actions(sets, "pick")


def enrich_sets(sets: pandas.DataFrame):
    sets["civ_draft_preset"] = sets["full_civ_draft"].map(
        lambda draft: draft["preset"]["name"], na_action="ignore"
    )
    sets["map_draft_preset"] = sets["full_map_draft"].map(
        lambda draft: draft["preset"]["name"], na_action="ignore"
    )
    if "civ_draft_host" not in sets.columns:
        sets["civ_draft_host"] = pandas.NA
    if "civ_draft_guest" not in sets.columns:
        sets["civ_draft_guest"] = pandas.NA
    if "map_draft_host" not in sets.columns:
        sets["map_draft_host"] = pandas.NA
    if "map_draft_guest" not in sets.columns:
        sets["map_draft_guest"] = pandas.NA
    sets.loc[pandas.isna(sets["civ_draft_host"]), "civ_draft_host"] = sets.loc[
        pandas.isna(sets["civ_draft_host"]), "full_civ_draft"
    ].map(lambda draft: draft["nameHost"], na_action="ignore")
    sets.loc[pandas.isna(sets["civ_draft_guest"]), "civ_draft_guest"] = sets.loc[
        pandas.isna(sets["civ_draft_guest"]), "full_civ_draft"
    ].map(lambda draft: draft["nameGuest"], na_action="ignore")
    sets.loc[pandas.isna(sets["map_draft_host"]), "map_draft_host"] = sets.loc[
        pandas.isna(sets["map_draft_host"]), "full_map_draft"
    ].map(lambda draft: draft["nameHost"], na_action="ignore")
    sets.loc[pandas.isna(sets["map_draft_guest"]), "map_draft_guest"] = sets.loc[
        pandas.isna(sets["map_draft_guest"]), "full_map_draft"
    ].map(lambda draft: draft["nameGuest"], na_action="ignore")
    sets["draft_errors"] = sets.apply(check_errors, axis=1)
    return sets


def main():
    tournament = sys.argv[1]
    if len(sys.argv) < 3:
        action = "partial"
    else:
        action = sys.argv[2]
    os.chdir(os.path.join("tournaments", tournament))
    if action == "partial":
        sets = pandas.read_parquet("sets.parquet")
        sets = fetch_missing_drafts(sets)
    elif action == "full":
        sets = pandas.read_csv("sets.csv")
        sets = fetch_drafts(sets)
    elif action == "add":
        csv_sets = pandas.read_csv("sets.csv")
        sets = pandas.read_parquet("sets.parquet")
        index_cols = ["player1", "player2", "bracket", "stage"]
        sets = (
            pandas.concat([sets.dropna(subset=["player1", "player2"]), csv_sets])
            .drop_duplicates(index_cols)
            .reset_index()
            .drop("index", axis="columns")
        )
        sets = fetch_missing_drafts(sets)
    else:
        print(f"Unknown action '{action}'. Allowed actions: 'partial', 'full', 'add'")
        sys.exit(-1)
    enriched_sets = enrich_sets(sets)
    # print_draft_mistakes_stats(enriched_sets)
    # print_civ_stats(enriched_sets)
    # print_map_stats(enriched_sets)


if __name__ == "__main__":
    main()
