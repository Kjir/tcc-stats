import os
import sys
import pandas
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.ticker as mticker
from hcc.civilizations import civilizations
from hcc.players import rename_alias
from hcc.tournament import get_tournament_bracket_ranks


def load_data():
    sets = pandas.read_json("sets_and_games.json")
    sets["map"] = (
        sets["map"]
        .map(lambda x: "Fractal" if x == "OeCoC Fractal" else x)
        .map(lambda x: "Coast to Mountain" if x == "TTL3 Coast to Mountain" else x)
        .map(lambda x: x.replace("TCC ", "") if pandas.notna(x) else x)
    )
    sets["player1_game"] = sets["player1_game"].map(rename_alias, na_action="ignore")
    sets["player2_game"] = sets["player2_game"].map(rename_alias, na_action="ignore")
    pandas.to_datetime(
        sets[pandas.notna(sets["duration"])]["duration"], unit="ms"
    )  # This fixes an issue in the next line?
    sets["duration"] = pandas.to_datetime(sets["duration"], unit="ms")
    return sets


def setup_fig(title: str):
    sns.set_theme("notebook")
    sns.set_palette("Paired")
    f = plt.figure(figsize=(16, 9))
    f.suptitle(title)
    return f


bracket_rank: dict[str, int] | None = None


def get_bracket_rank(bracket: str):
    global bracket_rank
    if bracket_rank is None:
        bracket_rank = get_tournament_bracket_ranks()
    return bracket_rank.get(bracket, -1)


def plot_eapm():
    sets = load_data()
    eapm = sets[["bracket", "eapm1"]].rename({"eapm1": "eapm"}, axis="columns")
    eapm = pandas.concat(
        [eapm, sets[["bracket", "eapm2"]].rename({"eapm2": "eapm"}, axis="columns")]
    )
    eapm = eapm[pandas.notna(eapm["eapm"])]
    eapm["order"] = eapm["bracket"].map(get_bracket_rank)
    f = setup_fig("Effective APM per Bracket")
    sns.stripplot(data=eapm.sort_values("order"), x="bracket", y="eapm", hue="bracket")
    f.savefig("plots/eapm.png")


def plot_player_eapm(player: str):
    sets = load_data()
    bracket = sets.bracket[sets["player1_set"].str.lower() == player.lower()].iloc[0]
    eapm = sets[["player1_game", "bracket", "eapm1"]].rename(
        {"player1_game": "player", "eapm1": "eapm"}, axis="columns"
    )
    eapm = pandas.concat(
        [
            eapm,
            sets[["player2_game", "bracket", "eapm2"]].rename(
                {"player2_game": "player", "eapm2": "eapm"}, axis="columns"
            ),
        ]
    )
    eapm = eapm[pandas.notna(eapm["eapm"])]
    f = setup_fig(f"Effective APM for {player} in bracket {bracket}")
    g = sns.histplot(
        data=eapm.loc[eapm["bracket"] == bracket]
        .groupby("player")
        .agg({"eapm": "mean"})
    )
    g.axvline(
        x=eapm.eapm[eapm["player"].str.lower() == player.lower()].mean(),
        color="b",
        dashes=(5, 2),
    )
    f.savefig(f"plots/eapm-bracket-{player}.png")
    f = setup_fig(f"Effective APM for {player} all brackets")
    g = sns.histplot(data=eapm.groupby("player").agg({"eapm": "mean"}))
    g.axvline(
        x=eapm.eapm[eapm["player"].str.lower() == player.lower()].mean(),
        color="b",
        dashes=(5, 2),
    )
    f.savefig(f"plots/eapm-overall-{player}.png")


def plot_duration():
    sets = load_data()
    duration = sets[["map", "duration", "bracket"]]
    duration = duration[pandas.notna(duration["duration"])]
    map_counts = sets.value_counts("map")
    duration["order"] = duration["map"].map(lambda b: map_counts[b])
    duration = duration.sort_values("order", ascending=False)
    f = setup_fig("Game duration by Map")
    dtFmt = mdates.DateFormatter("%H:%M:%S")  # define the formatting
    plt.gca().yaxis.set_major_formatter(dtFmt)  # apply the format to the desired axis
    print(duration)
    sns.stripplot(data=duration, x="map", y="duration", hue="map")
    f.savefig("plots/duration.png")


def plot_maps_played():
    sets = load_data()
    f = setup_fig("Maps played - Overall")
    sns.barplot(
        y="map",
        x="count",
        data=sets.value_counts("map").sort_values(ascending=False).reset_index(),
        color="b",
    )
    f.savefig("plots/maps-played.png")
    plt.close()
    for bracket in sets["bracket"].unique():
        print(f"Plotting {bracket}")
        f = setup_fig(f"Maps played - {bracket}")
        sns.barplot(
            y="map",
            x="count",
            data=sets[sets["bracket"] == bracket]
            .value_counts("map")
            .sort_values(ascending=False)
            .reset_index(),
            color="b",
        )
        f.savefig(f"plots/maps-played-{bracket}.png")
        plt.close()


def plot_civs_played():
    sets = load_data()
    civs = sets[["bracket", "civ1"]].rename({"civ1": "civ"}, axis="columns")
    civs = pandas.concat(
        [civs, sets[["bracket", "civ2"]].rename({"civ2": "civ"}, axis="columns")]
    )
    f = setup_fig("Civs played - Overall")
    zero_civs = pandas.Series(0, index=civilizations, name="count")
    zero_civs.index.name = "civ"
    civ_counts = zero_civs + civs.value_counts("civ")
    sns.barplot(
        y="civ",
        x="count",
        data=civ_counts.sort_values(ascending=False).reset_index(),
        color="b",
    )
    f.savefig("plots/civs-played.png")
    plt.close()

    for bracket in civs["bracket"].unique():
        print(f"Plotting {bracket}")
        f = setup_fig(f"Civs played - {bracket}")
        civ_counts = zero_civs + civs[civs["bracket"] == bracket].value_counts("civ")
        sns.barplot(
            y="civ",
            x="count",
            data=civ_counts.sort_values(ascending=False).reset_index(),
            color="b",
        )
        f.savefig(f"plots/civs-played-{bracket}.png")
        plt.close()


def plot_civ_win_rate():
    sets = load_data()
    civs = (
        sets[["bracket", "civ1", "player1_game", "winner"]]
        .rename({"civ1": "civ"}, axis="columns")
        .assign(win=lambda x: (x["winner"] == x["player1_game"]).astype("int"))[
            ["bracket", "civ", "win"]
        ]
    )
    civs = pandas.concat(
        [
            civs,
            sets[["bracket", "civ2", "player2_game", "winner"]]
            .rename({"civ2": "civ"}, axis="columns")
            .assign(win=lambda x: (x["winner"] == x["player2_game"]).astype("int"))[
                ["bracket", "civ", "win"]
            ],
        ]
    )
    civ_wins = (
        civs[["civ", "win"]]
        .groupby("civ")
        .agg(count=("win", "count"), win=("win", "sum"))
        .assign(rate=lambda c: c["win"] / c["count"] * 100)
    )
    print(civ_wins)
    print(sum(civ_wins["count"]))
    print(sum(civ_wins["win"]))
    f = setup_fig("Civ win rate - Overall")
    plt.gca().xaxis.set_major_formatter(mticker.PercentFormatter())
    sns.barplot(
        y="civ",
        x="rate",
        data=civ_wins.sort_values("rate", ascending=False).reset_index(),
    )
    f.savefig("plots/civ-win-rate.png")
    # plt.close()
    #
    # for bracket in civs['bracket'].unique():
    #     print(f"Plotting {bracket}")
    #     f = setup_fig(f"Civs played - {bracket}")
    #     civ_counts = zero_civs + civs[civs['bracket'] == bracket].value_counts("civ")
    #     sns.barplot(
    #         y="civ",
    #         x="count",
    #         data=civ_counts.sort_values(ascending=False).reset_index(),
    #         color="b",
    #     )
    #     f.savefig(f"plots/civs-played-{bracket}.png")
    #     plt.close()


if __name__ == "__main__":
    tournament = sys.argv[1]
    os.chdir(os.path.join("tournaments", tournament))
    command = sys.argv[2]
    if not os.path.exists("plots"):
        os.mkdir("plots")
    if command == "eapm":
        if len(sys.argv) == 3:
            plot_eapm()
        else:
            plot_player_eapm(sys.argv[3])
    if command == "duration":
        plot_duration()
    if command == "maps-played":
        plot_maps_played()
    if command == "civs-played":
        plot_civs_played()
    if command == "civ-win-rate":
        plot_civ_win_rate()
    if command == "all":
        plot_eapm()
        plot_duration()
        plot_maps_played()
        plot_civs_played()
        plot_civ_win_rate()
