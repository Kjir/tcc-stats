import os
import pandas
import sys
import mgz.model


unknown_unit_fixes = {
    1735: "Urumi Swordsman",
    1754: "Caravanserai",
    1786: "Spearman",
    1790: "Centurion",
    1795: "Dromon",
    1808: "Mule Cart",
}

unknown_tech_fixes = {
    28: "Bimaristan",
    46: "Devotion",
    454: "Counterweights",
    455: "Detinets",
    526: "Savar",
    782: "Szlachta Privileges",
    783: "Lechitic Legacy",
    784: "Wagenburg Tactics",
    785: "Hussite Reforms",
    828: "Elite Ratha",
    830: "Elite Chakram Thrower",
    831: "Medical Corps",
    832: "Wootz Steel",
    833: "Paiks",
    834: "Mahayana",
    835: "Kshatriyas",
    836: "Frontier Guards",
    838: "Siege Elephant",
    840: "Elite Ghulam",
    843: "Elite Shrivamsha Rider",
    875: "Gambesons",
    882: "Elite Centurion",
    883: "Ballistas",
    884: "Comitatenses",
    885: "Legionary",
    918: "Elite Composite Bowman",
    920: "Elite Monaspa",
    921: "Fereters",
    922: "Cilician Fleet",
    923: "Svan Towers",
    924: "Aznauri Cavalry",
}


def parse_game(game_file: str):
    with open(game_file, "rb") as savegame:
        return mgz.model.parse_match(savegame)


def find_games(starting_dir: str, only_subdirs: list | None = None):
    for set, subdirs, games in os.walk(starting_dir):
        if set == starting_dir and len(subdirs):
            continue
        if only_subdirs is not None and os.path.basename(set) not in only_subdirs:
            continue
        # else:
        # print(f"Parsing set {os.path.basename(set)}")
        games = sorted(games)
        for game in games:
            if (
                game == "metadata.json"
                or game.endswith("recover.yaml")
                or game.endswith(".skip")
            ):
                continue
            savegame = os.path.join(set, game)
            if os.path.isfile(f"{savegame}.skip"):
                print(f"Skipping {game} due to skip file")
                continue
            try:
                yield parse_game(savegame)
            except KeyError as e:
                if str(e) == "'leaderboards'":
                    continue
                raise
            except Exception as e:
                print(f"Error: {e} ({savegame})")
                continue


def get_researched_technologies(game: mgz.model.Match):
    technologies: list[pandas.DataFrame] = []
    for action in game.actions:
        if action.type != mgz.model.ActionEnum.RESEARCH:
            continue
        if (
            not action.payload["technology"]
            and unknown_tech_fixes.get(action.payload["technology_id"]) is None
        ):
            print(
                f"Unknown technology ({action.payload['technology_id']}) by {action.player.name} ({action.player.civilization}) queued at {action.timestamp} in {game.players[0].name} vs {game.players[1].name} on {game.map.name}: {action.payload}"
            )
        technologies.append(
            pandas.DataFrame(
                {
                    "timestamp": game.timestamp,
                    "map": game.map.name,
                    "player1": game.players[0].name,
                    "player2": game.players[1].name,
                    "duration": game.duration,
                    "queuer": action.player.name,
                    "technology": action.payload["technology"]
                    if action.payload["technology"]
                    else unknown_tech_fixes.get(
                        action.payload["technology_id"],
                        f"<unknown:{action.payload['technology_id']}>",
                    ),
                    "time": action.timestamp,
                },
                index=["timestamp"],
            )
        )
    return technologies


def get_queued_units(game: mgz.model.Match):
    units: list[pandas.DataFrame] = []
    for action in game.actions:
        if action.type != mgz.model.ActionEnum.DE_QUEUE:
            continue
        if (
            not action.payload["unit"]
            and unknown_unit_fixes.get(action.payload["unit_id"]) is None
        ):
            print(
                f"Unknown unit ({action.payload['unit_id']}) by {action.player.name} ({action.player.civilization}) queued at {action.timestamp} in {game.players[0].name} vs {game.players[1].name} on {game.map.name}: {action.payload}"
            )
        units.append(
            pandas.DataFrame(
                {
                    "timestamp": game.timestamp,
                    "map": game.map.name,
                    "player1": game.players[0].name,
                    "player2": game.players[1].name,
                    "duration": game.duration,
                    "queuer": action.player.name,
                    "amount": action.payload["amount"],
                    "unit": action.payload["unit"]
                    if action.payload["unit"]
                    else unknown_unit_fixes.get(
                        action.payload["unit_id"],
                        f"<unknown:{action.payload['unit_id']}>",
                    ),
                    "time": action.timestamp,
                },
                index=["timestamp"],
            )
        )
    return units


def get_buildings(game: mgz.model.Match):
    buildings: list[pandas.DataFrame] = []
    for action in game.actions:
        if action.type != mgz.model.ActionEnum.BUILD:
            continue
        if (
            not action.payload["building"]
            and unknown_unit_fixes.get(action.payload["building_id"]) is None
        ):
            print(
                f"Unknown building ({action.payload['building_id']}) by {action.player.name} ({action.player.civilization}) queued at {action.timestamp} in {game.players[0].name} vs {game.players[1].name} on {game.map.name}: {action.payload}"
            )
        buildings.append(
            pandas.DataFrame(
                {
                    "timestamp": game.timestamp,
                    "map": game.map.name,
                    "player1": game.players[0].name,
                    "player2": game.players[1].name,
                    "duration": game.duration,
                    "queuer": action.player.name,
                    "building": action.payload["building"]
                    if action.payload["building"]
                    else unknown_unit_fixes.get(
                        action.payload["building_id"],
                        f"<unknown:{action.payload['building_id']}>",
                    ),
                    "time": action.timestamp,
                },
                index=["timestamp"],
            )
        )
    return buildings


def scan_actions():
    buildings = []
    units = []
    technologies = []
    for game in find_games("games"):
        buildings.extend(get_buildings(game))
        units.extend(get_queued_units(game))
        technologies.extend(get_researched_technologies(game))
    units = pandas.concat(units).sort_values(["timestamp"])
    units["unit"] = units.unit.astype("category")
    technologies = pandas.concat(technologies)
    technologies["technology"] = technologies.technology.astype("category")
    buildings = pandas.concat(buildings)
    buildings["building"] = buildings["building"].astype("category")
    units.to_parquet("units.parquet")
    technologies.to_parquet("technologies.parquet")
    buildings.to_parquet("buildings.parquet")
    return [units, technologies, buildings]


def read_actions():
    units = pandas.read_parquet("units.parquet")
    technologies = pandas.read_parquet("technologies.parquet")
    buildings = pandas.read_parquet("buildings.parquet")
    return [units, technologies, buildings]


if __name__ == "__main__":
    tournament = sys.argv[1]
    action = sys.argv[2]
    os.chdir(os.path.join("tournaments", tournament))
    if action == "scan":
        [units, technologies, buildings] = scan_actions()
    elif action == "print":
        [units, technologies, buildings] = read_actions()
    else:
        print(f"Don't know what to do with {sys.argv[1]}")
        sys.exit(-1)
    print("Created units:")
    print(
        units.groupby("unit", observed=False)
        .agg({"amount": "sum"})
        .sort_values("amount", ascending=False)
    )
    trade = units[units["unit"] == "Trade Cart"]
    print("Trade carts:\n")
    print(
        trade.groupby(
            ["timestamp", "map", "player1", "player2", "duration", "queuer", "unit"],
            observed=True,
        ).agg({"amount": "sum"})
    )
    print(trade.groupby(["queuer"]).agg({"amount": "sum"}))
    print("\nVillagers:\n")
    vils = units[units["unit"] == "Villager"]
    print(
        vils.groupby(
            ["timestamp", "map", "player1", "player2", "duration", "queuer"]
        ).agg({"amount": "sum"})
    )
    print("\nMost queued units:\n")
    other_units = units[units["unit"] != "Villager"]
    print(
        other_units.groupby(
            ["timestamp", "map", "player1", "player2", "queuer", "unit"],
            observed=True,
        )
        .agg({"amount": "sum"})
        .sort_values(["player1", "player2", "map", "queuer", "amount"], ascending=False)
        .groupby(["timestamp", "map", "player1", "player2", "queuer"])
        .head(3)
    )
    print("\nTechnology - Age Up\n")
    print(
        technologies[
            technologies.technology.map(
                lambda t: t in ["Feudal Age", "Castle Age", "Imperial Age"]
            )
        ]
        .sort_values(["timestamp", "map", "player1", "player2", "time"])[
            ["map", "queuer", "technology", "time"]
        ]
        .set_index(["queuer"])
    )
