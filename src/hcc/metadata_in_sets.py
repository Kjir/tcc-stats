from dataclasses import dataclass
import os.path
import sys
import json
import pandas

from hcc.enrich_sets import extract_draft_id
from hcc.get_civs import get_civs


@dataclass
class DraftPlayersAndLinks:
    host_name: str
    guest_name: str
    map_host: str
    map_guest: str
    civ_host: str
    civ_guest: str
    map_draft: str
    civ_draft: str


def name_matches(players: pandas.Series, name):
    return players.str.lower() == name.lower()


def get_from_draft_url():
    print("Draft information is missing in the metadata file")
    map_draft_url = input("Map draft: ")
    civ_draft_url = input("Civ draft: ")
    map_draft_id = extract_draft_id(map_draft_url)
    civ_draft_id = extract_draft_id(civ_draft_url)
    map_draft = get_civs(map_draft_id)
    civ_draft = get_civs(civ_draft_id)
    map_host = map_draft["nameHost"]
    map_guest = map_draft["nameGuest"]
    civ_host = civ_draft["nameHost"]
    civ_guest = civ_draft["nameGuest"]
    print(f"Fetched players: {map_host} vs {map_guest} - {civ_host} vs {civ_guest}")
    return (
        map_host,
        map_guest,
        civ_host,
        civ_guest,
        map_draft_id,
        civ_draft_id,
    )


def get_host_and_guest(metadata_file):
    with open(metadata_file, "r") as meta_file:
        metadata = json.load(meta_file)
    return DraftPlayersAndLinks(
        metadata["host_name"],
        metadata["guest_name"],
        metadata["maps"]["host_name"],
        metadata["maps"]["guest_name"],
        metadata["civs"]["host_name"],
        metadata["civs"]["guest_name"],
        metadata["maps"]["draft"].strip(),
        metadata["civs"]["draft"].strip(),
    )


def read_metadata(metadata_file):
    sets_file = "sets.csv"
    sets_parquet = "sets.parquet"
    sets = pandas.read_csv(sets_file)
    psets = pandas.read_parquet(sets_parquet)
    sets["map_draft"] = sets["map_draft"].astype("string")
    sets["civ_draft"] = sets["civ_draft"].astype("string")
    base_filter = pandas.notna(sets["player1"]) & pandas.notna(sets["player2"])

    draft_info = get_host_and_guest(metadata_file)

    host_filter = name_matches(sets["player1"], draft_info.host_name)
    reverse_host_filter = name_matches(sets["player1"], draft_info.guest_name)
    guest_filter = name_matches(sets["player2"], draft_info.guest_name)
    reverse_guest_filter = name_matches(sets["player2"], draft_info.host_name)
    if len(sets[base_filter & host_filter]) == 0:
        print(f'Could not find host "{draft_info.host_name}"')
    if len(sets[base_filter & guest_filter]) == 0:
        print(f'Could not find guest "{draft_info.guest_name}"')
    match_filter = base_filter & host_filter & guest_filter
    reverse_match_filter = base_filter & reverse_host_filter & reverse_guest_filter
    correct_matches = sets[match_filter]
    reverse_matches = sets[reverse_match_filter]
    matched_set = None
    if len(correct_matches) + len(reverse_matches) == 2:
        print("This is a rematch in the knockout bracket")
        match_filter = match_filter & ~sets["stage"].str.startswith("Group")
        reverse_match_filter = reverse_match_filter & ~sets["stage"].str.startswith(
            "Group"
        )
        correct_matches = sets[match_filter]
        reverse_matches = sets[reverse_match_filter]
    if len(correct_matches) == 1:
        matched_set = correct_matches
    if len(reverse_matches) == 1:
        print("Host and Guest were switched around in the drafts.")
        matched_set = reverse_matches
        match_filter = reverse_match_filter

    if matched_set is None:
        print(
            f'Could not find a match for "{draft_info.host_name}" and "{draft_info.guest_name}"'
        )
        raise Exception("Could not find a match")
    sets.loc[match_filter, "map_draft"] = draft_info.map_draft
    sets.loc[match_filter, "civ_draft"] = draft_info.civ_draft
    psets.loc[match_filter, "map_draft"] = draft_info.map_draft
    psets.loc[match_filter, "civ_draft"] = draft_info.civ_draft

    if "map_draft_host" not in psets.columns or "map_draft_host" not in sets.columns:
        sets["map_draft_host"] = None
        psets["map_draft_host"] = None
    if "map_draft_guest" not in psets.columns or "map_draft_guest" not in sets.columns:
        sets["map_draft_guest"] = None
        psets["map_draft_guest"] = None
    sets.loc[match_filter, "map_draft_host"] = draft_info.map_host
    psets.loc[match_filter, "map_draft_host"] = draft_info.map_host
    sets.loc[match_filter, "map_draft_guest"] = draft_info.map_guest
    psets.loc[match_filter, "map_draft_guest"] = draft_info.map_guest

    if "civ_draft_host" not in sets.columns or "civ_draft_host" not in psets.columns:
        sets["civ_draft_host"] = None
        psets["civ_draft_host"] = None
    if "civ_draft_guest" not in sets.columns or "civ_draft_guest" not in psets.columns:
        sets["civ_draft_guest"] = None
        psets["civ_draft_guest"] = None
    sets.loc[match_filter, "civ_draft_host"] = draft_info.civ_host
    psets.loc[match_filter, "civ_draft_host"] = draft_info.civ_host
    sets.loc[match_filter, "civ_draft_guest"] = draft_info.civ_guest
    psets.loc[match_filter, "civ_draft_guest"] = draft_info.civ_guest

    sets.to_csv(sets_file, header=True, index=False)
    psets.to_parquet(sets_parquet)


if __name__ == "__main__":
    tournament = sys.argv[1]
    os.chdir(os.path.join("tournaments", tournament))
    set_dir = sys.argv[2]
    read_metadata(os.path.join("games", set_dir, "metadata.json"))
