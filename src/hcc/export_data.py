import os
import sys
from pathlib import Path
import pandas
import json


def extract_drafts(sets_and_games: pandas.DataFrame, draft_type: str) -> list[dict]:
    draft = f"{draft_type}_draft"
    full_draft = f"full_{draft}"
    return (
        sets_and_games[pandas.notna(sets_and_games[full_draft])][
            ["bracket", full_draft, draft, "stage"]
        ]
        .groupby(draft)
        .head(1)
        .assign(
            draft=lambda row: row[full_draft].map(
                lambda draft: [
                    {
                        "action": event["actionType"],
                        "map": event["chosenOptionId"],
                        "type": "admin"
                        if event["executingPlayer"] == "NONE"
                        else "player",
                    }
                    for event in draft["events"]
                    if "actionType" in event
                ],
                na_action="ignore",
            )
        )
        .apply(
            lambda row: {
                "bracket": row["bracket"],
                "draft": row["draft"],
                "draftId": row[draft],
                "stage": row["stage"],
            },
            axis="columns",
        )
        .to_list()
    )


def export_games(sets_and_games):
    return (
        sets_and_games[pandas.notna(sets_and_games["map_draft"])][
            [
                "bracket",
                "civ_draft",
                "duration",
                "map",
                "map_draft",
                "stage",
                "week",
                "winner",
                "player1_game",
                "player2_game",
                "civ1",
                "civ2",
                "eapm1",
                "eapm2",
            ]
        ]
        .rename({"civ_draft": "civDraftId", "map_draft": "mapDraftId"}, axis="columns")
        .assign(
            players=lambda df: df[["player1_game", "player2_game"]].apply(
                lambda row: [row["player1_game"], row["player2_game"]], axis="columns"
            ),
            winningCiv=lambda df: df["civ1"].where(
                df["winner"] == df["player1_game"], df["civ2"]
            ),
            losingCiv=lambda df: df["civ1"].where(
                df["winner"] != df["player1_game"], df["civ2"]
            ),
            week=lambda df: df["week"].astype("float"),
            map=lambda df: df["map"],
            eapm=lambda df: df[["eapm1", "eapm2"]].apply(
                lambda row: [row["eapm1"], row["eapm2"]], axis="columns"
            ),
        )[
            [
                "bracket",
                "players",
                "civDraftId",
                "duration",
                "losingCiv",
                "map",
                "mapDraftId",
                "stage",
                "week",
                "winningCiv",
                "eapm",
            ]
        ]
    )


def export_data():
    sets_and_games = pandas.read_json("sets_and_games.json")
    export_games(sets_and_games).to_json("games.json", orient="records")
    with Path("drafts.json").open("w") as f:
        drafts = {
            "civDrafts": extract_drafts(sets_and_games, "civ"),
            "mapDrafts": extract_drafts(sets_and_games, "map"),
        }
        json.dump(drafts, f)


if __name__ == "__main__":
    tournament = sys.argv[1]
    os.chdir(os.path.join("tournaments", tournament))
    export_data()
