import json
import os
from pathlib import Path
import sys
import pandas
from hcc.parse_games import find_games
from hcc.players import FindOrAddResult, find_or_add_alias, rename_alias
from hcc.tournament import get_tournament_maps


def remove_extra_games(game):
    iswinner = lambda player: "W" if game["winner"] == player else "L"
    print(
        f"{game['player1']} [{iswinner(game['player1'])}] vs {game['player2']} [{iswinner(game['player2'])}] on {game['map']} - {game['file']}"
    )
    path = os.path.join(game["set"], game["file"])
    remove_hint = f"Remove game file ({path}) [y/N]? "
    should_remove = input(remove_hint)
    if should_remove.lower() == "y":
        remove_game(path)


def get_winner(game):
    if game["winner"] is not None:
        return game["winner"]
    print("No winner found:")
    print(f"{game['player1']} vs {game['player2']} on {game['map']} - {game['file']}")
    # path = os.path.join(game["set"], game["file"])
    winner_hint = f"Pick a winner: [1] {game['player1']} — [2] {game['player2']} "
    winner = input(winner_hint)
    if winner == "1":
        return game["player1"]
    if winner == "2":
        return game["player2"]
    raise ValueError(f"Unrecognized winner: {winner}. Choose 1 or 2")


def remove_game(path: str):
    os.unlink(path)
    print("Game removed")


def adjust_games(games: pandas.DataFrame):
    restored_games = games.groupby(
        ["player1", "player2", "map", "civ1", "civ2"]
    ).filter(lambda x: len(x) > 1)
    if len(restored_games) > 0:
        last_games = []
        for map_name in restored_games["map"].unique():
            map_games = restored_games[restored_games["map"] == map_name].sort_values(
                "date"
            )
            last_game = (
                map_games.sort_values("date")
                .tail(1)
                .assign(
                    date=map_games["date"].min(),
                    winner=map_games["winner"].array[-1],
                    eapm1=map_games["eapm1"].max(),
                    eapm2=map_games["eapm2"].max(),
                    duration=map_games["duration"].max(),
                )
            )
            last_games.append(last_game)
        games = (
            pandas.concat([games, *last_games])
            .drop_duplicates(
                ["player1", "player2", "civ1", "civ2", "map", "set"],
                keep="last",
            )
            .sort_values("date")
        )
    games["winner"] = games.apply(get_winner, axis="columns")

    return games


def remove_dummy_games(dir, games=None):
    if games is None:
        games = sorted(
            [game for game in os.listdir(dir) if game.endswith(".aoe2record")],
            reverse=True,
        )
    dummy_candidate = games[0]
    games_to_check = games[1:]
    for game in games_to_check:
        if (
            Path(dir, dummy_candidate)
            .read_bytes()
            .startswith(Path(dir, game).read_bytes())
        ):
            Path(dir, dummy_candidate).unlink()
            print(f"Removing dummy game {dummy_candidate}")
            break
    if len(games_to_check) > 1:
        return remove_dummy_games(dir, games_to_check)


def check_game_info(game):
    if game["winner"] is None:
        raise Exception(f"Found no winner in game {game['file']}")
    maps = get_tournament_maps()
    if game["map"] not in maps:
        print(f"\nUnknown map \"{game['map']}\" in game {game['file']}")


if __name__ == "__main__":
    is_removed = lambda x: not (
        os.path.isdir(x["set"]) and os.path.isfile(os.path.join(x["set"], x["file"]))
    )

    tournament = sys.argv[1]
    os.chdir(os.path.join("tournaments", tournament))
    games_parquet = "games.parquet"
    games_dir = "games"
    set_dir = os.path.join(games_dir, sys.argv[2])
    with open(f"{set_dir}/metadata.json", "r") as metadata_file:
        metadata = json.load(metadata_file)
    remove_dummy_games(set_dir)
    parsed_games: pandas.DataFrame = find_games(games_dir, [sys.argv[2]])
    parsed_games["host_name"] = metadata["host_name"]
    parsed_games["guest_name"] = metadata["guest_name"]
    parsed_games = adjust_games(parsed_games)
    parsed_games.apply(remove_extra_games, axis="columns")
    parsed_games = parsed_games[~parsed_games.apply(is_removed, axis="columns")]
    parsed_games.apply(check_game_info, axis="columns")
    if os.path.isfile(games_parquet):
        games: pandas.DataFrame = pandas.read_parquet(games_parquet)

        if "host_name" not in games.columns or "guest_name" not in games.columns:

            def rename_player(row, player):
                print(f"{row['player1']} vs {row['player2']} ({row['set']})")
                return rename_alias(row[player])

            games["host_name"] = games.apply(
                lambda row: rename_player(row, "player1"), axis="columns"
            )
            games["guest_name"] = games.apply(
                lambda row: rename_player(row, "player2"), axis="columns"
            )

        removed_games = games.apply(
            is_removed,
            axis="columns",
        )

        games = pandas.concat(
            [
                games[~games["set"].isin(parsed_games["set"]) & ~removed_games],
                parsed_games,
            ]
        )
    else:
        games = parsed_games

    games.to_parquet(games_parquet)
    print("")
    print(parsed_games)
