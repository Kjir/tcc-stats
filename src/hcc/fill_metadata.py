from dataclasses import dataclass
import json
import os
import sys
from pathlib import Path
from typing import Any

from hcc.enrich_sets import extract_draft_id
from hcc.get_civs import get_civs
from hcc.players import FindOrAddResult, find_or_add_alias, rename_alias


@dataclass
class DownloadedDraft:
    draft_id: str
    draft_json: dict[str, Any]
    host: str
    guest: str


@dataclass
class DraftInfo:
    maps: DownloadedDraft
    civs: DownloadedDraft


def get_from_draft_url():
    print("Draft information is missing in the metadata file")
    map_draft_url = input("Map draft: ")
    civ_draft_url = input("Civ draft: ")
    map_draft_id = extract_draft_id(map_draft_url)
    civ_draft_id = extract_draft_id(civ_draft_url)
    map_draft: dict[str, Any] = get_civs(map_draft_id)
    civ_draft: dict[str, Any] = get_civs(civ_draft_id)
    map_host: str = map_draft["nameHost"]
    map_guest: str = map_draft["nameGuest"]
    civ_host: str = civ_draft["nameHost"]
    civ_guest: str = civ_draft["nameGuest"]
    print(f"Fetched players: {map_host} vs {map_guest} - {civ_host} vs {civ_guest}")
    return DraftInfo(
        maps=DownloadedDraft(
            draft_id=map_draft_id, draft_json=map_draft, host=map_host, guest=map_guest
        ),
        civs=DownloadedDraft(
            draft_id=civ_draft_id, draft_json=civ_draft, host=civ_host, guest=civ_guest
        ),
    )


def fill_metadata(metadata_file: Path):
    if not metadata_file.exists():
        _ = metadata_file.write_text("{}")
    metadata: dict[str, Any] = json.loads(metadata_file.read_text())
    if (
        "maps" not in metadata
        or "civs" not in metadata
        or metadata["maps"] is None
        or metadata["civs"] is None
    ):
        metadata = {**metadata, **fill_draft_info()}
    metadata = identify_host_and_guest(metadata)
    metadata_file.write_text(json.dumps(metadata))


def identify_host_and_guest(metadata: dict[str, Any]):
    # maps draft: check if we find a match for the players.
    map_host: str = metadata["maps"]["host"]
    map_guest: str = metadata["maps"]["guest"]
    civ_host: str = metadata["civs"]["host"]
    civ_guest: str = metadata["civs"]["guest"]
    map_host_result = rename_alias(map_host)
    map_guest_result = rename_alias(map_guest)
    result = find_or_add_alias(map_host, map_guest_result)
    if result == FindOrAddResult.OTHER_ACTION:
        sys.exit(0)
    if result == FindOrAddResult.ADDED:
        map_host_result = rename_alias(map_host)
    result = find_or_add_alias(map_guest, map_host_result)
    if result == FindOrAddResult.OTHER_ACTION:
        sys.exit(0)
    if result == FindOrAddResult.ADDED:
        map_guest_result = rename_alias(map_guest)
    civ_host_result = None
    civ_guest_result = None
    if civ_host == map_host:
        civ_host_result = map_host_result
    if civ_host == map_guest:
        civ_host_result = map_guest_result
    if civ_guest == map_guest:
        civ_guest_result = map_guest_result
    if civ_guest == map_host:
        civ_guest_result = map_host_result
    if civ_host_result is None:
        civ_host_result = rename_alias(civ_host)
    if civ_guest_result is None:
        civ_guest_result = rename_alias(civ_guest)

    metadata["host_name"] = map_host_result
    metadata["guest_name"] = map_guest_result
    metadata["maps"]["host_name"] = map_host_result
    metadata["maps"]["guest_name"] = map_guest_result
    metadata["civs"]["host_name"] = civ_host_result
    metadata["civs"]["guest_name"] = civ_guest_result
    return metadata


def fill_draft_info():
    draft_info = get_from_draft_url()
    return {
        "maps": {
            "draft": draft_info.maps.draft_id,
            "preset": draft_info.maps.draft_json["preset"]["presetId"],
            "host": draft_info.maps.host,
            "guest": draft_info.maps.guest,
            "availableMaps": {
                option["name"]: {
                    "name": option["name"],
                    "image": option["imageUrls"]["unit"],
                }
                for option in draft_info.maps.draft_json["preset"]["draftOptions"]
            },
            "pickedMaps": [
                event["chosenOptionId"]
                for event in draft_info.maps.draft_json["events"]
                if event.get("actionType", "") == "pick"
            ],
        },
        "civs": {
            "draft": draft_info.civs.draft_id,
            "preset": draft_info.civs.draft_json["preset"]["presetId"],
            "host": draft_info.civs.host,
            "guest": draft_info.civs.guest,
            "hostCivs": [
                event["chosenOptionId"]
                for event in draft_info.civs.draft_json["events"]
                if event.get("actionType", "") == "pick" and event["player"] == "HOST"
            ],
            "guestCivs": [
                event["chosenOptionId"]
                for event in draft_info.civs.draft_json["events"]
                if event.get("actionType", "") == "pick" and event["player"] == "GUEST"
            ],
        },
    }


if __name__ == "__main__":
    tournament = sys.argv[1]
    os.chdir(Path("tournaments", tournament))
    set_dir = sys.argv[2]
    fill_metadata(Path("games", set_dir, "metadata.json"))
