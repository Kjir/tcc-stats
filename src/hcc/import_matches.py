import sys
from pathlib import Path
import pandas

ttl_brackets = {
    "A": "Obsidian",
    "B": "Copper",
    "C": "Lead",
    "D": "Tin",
    "E": "Iron",
    "F": "Brick",
    "G": "Stone",
    "H": "Pebble",
    "I": "Ash",
    "J": "Mud",
    "K": "Dirt",
}


def import_matches(tournament: str, csv_file: Path):
    print(f"Importing {csv_file}")
    matches: pandas.DataFrame = pandas.read_csv(csv_file, header=1)
    # player1,player2,bracket,stage,week,map_draft,civ_draft,map_draft_host,map_draft_guest,civ_draft_host,civ_draft_guest
    matches = (
        matches[["🅰️ Player A", "🅱️ Player B", "🏰Bracket"]]
        .rename(
            {"🅰️ Player A": "player1", "🅱️ Player B": "player2", "🏰Bracket": "bracket"},
            axis="columns",
        )
        .dropna(subset=["player1", "player2"])
    )
    matches = matches.assign(
        bracket=matches["bracket"].map(lambda v: ttl_brackets[v]),
        stage="Group",
        week=1,
        map_draft=None,
        civ_draft=None,
        map_draft_host=None,
        map_draft_guest=None,
        civ_draft_host=None,
        civ_draft_guest=None,
    )
    matches.to_csv(
        Path("tournaments", tournament, "sets.csv"), index=False, header=True
    )


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Usage: import_matches.py <tournament> <csv_file>")
        sys.exit(-1)

    tournament = sys.argv[1]
    if not Path("tournaments", tournament, "tournament.yaml").exists():
        print(f"Unknown tournament: {tournament}")
        sys.exit(-2)
    import_matches(tournament, Path(sys.argv[2]))
