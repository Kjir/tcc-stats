import datetime
import os
import sys
import pandas
from mgz.summary import Summary
from mgz.util import Version
from mgz.reference import get_dataset
import yaml

_, civs = get_dataset(Version.DE, 0)
civs = civs["civilizations"]


def parse_game(game_file):
    with open(game_file, "rb") as savegame:
        summary = Summary(savegame)
        map = summary.get_map()
        players = summary.get_players()
        duration_raw = summary.get_duration()
        game_date = datetime.datetime.fromtimestamp(summary.get_played())
        settings = summary.get_settings()
    recover_info = {}
    recover_file = f"{game_file}.recover.yaml"
    if os.path.isfile(recover_file):
        with open(recover_file) as recover:
            recover_info = yaml.full_load(recover)
    winner = None
    if players[0]["winner"]:
        winner = players[0]["name"]
    elif players[1]["winner"]:
        winner = players[1]["name"]
    winner = recover_info.get("winner", winner)
    duration_raw = recover_info.get("duration", duration_raw)
    stats = {
        "date": [game_date],
        "file": [os.path.basename(game_file)],
        "map": [map["name"]],
        "size": [map["size"]],
        "dimension": [map["dimension"]],
        "player1": [players[0]["name"]],
        "civ1": [civs[str(players[0]["civilization"])]["name"]],
        "color1": [players[0]["color_id"]],
        "eapm1": [recover_info.get("eapm1", players[0]["eapm"])],
        "player2": [players[1]["name"]],
        "civ2": [civs[str(players[1]["civilization"])]["name"]],
        "color2": [players[1]["color_id"]],
        "eapm2": [recover_info.get("eapm2", players[1]["eapm"])],
        "duration": [datetime.timedelta(milliseconds=duration_raw)],
        "duration_raw": [duration_raw],
        "winner": [winner],
        "settings": [settings],
    }
    return pandas.DataFrame(data=stats)


def find_games(starting_dir: str, only_subdirs: list | None = None):
    tournament_games = []
    for tournament_set, subdirs, games in os.walk(starting_dir):
        if tournament_set == starting_dir and len(subdirs):
            continue
        if (
            only_subdirs is not None
            and os.path.basename(tournament_set) not in only_subdirs
        ):
            continue
        else:
            print(f"Parsing set {os.path.basename(tournament_set)}")
        games = sorted(games)
        set_games = []
        for game in games:
            if (
                game == "metadata.json"
                or game.endswith("recover.yaml")
                or game.endswith(".skip")
            ):
                continue
            savegame = os.path.join(tournament_set, game)
            if os.path.isfile(f"{savegame}.skip"):
                print(f"Skipping {game} due to skip file")
                continue
            try:
                game_stats = parse_game(savegame).assign(set=tournament_set)
                set_games.append(game_stats)
                tournament_games.append(game_stats)
            except RuntimeError as e:
                if str(e).startswith("invalid mgz file:"):
                    print(f"Could not parse game {savegame}")
                else:
                    raise
            except KeyError as e:
                if str(e) != "'leaderboards'":
                    raise
                set_games_t = pandas.concat(set_games)
                winners = set_games_t.value_counts("winner")
                print(winners)
                print(set_games_t)
                if winners.iloc[0] > set_games_t.shape[0] / 2:
                    print("Set was won early")
                    print(savegame)
                else:
                    print(f"Game is likely to be a placeholder: {savegame}")

    return pandas.concat(tournament_games)


if __name__ == "__main__":
    tournament = sys.argv[1]
    os.chdir(os.path.join("tournaments", tournament))
    games: pandas.DataFrame = find_games("games")
    games.to_parquet("games.parquet")
    # print(games)
